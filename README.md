# Description
BlaPPy is a webapp implemented in Python that allows alignment-based quality control of nanopore sequencing data.

Furthermore, it is possible to identify patterns in the data, search for further occurrences of these patterns and finally export all reads in which they occur.

# How to use BlaPPy
When running BlaPPy, first specify the path of the BAM file you want to analyze.
![](pictures/Import.png)
You can use the sidebar to navigate between pages, for example, to go to the _Statistics_ page. There you can switch between tabs and use the inputs to view different visualizations of the underlying data.
![](pictures/Statistics.png)
When hovering the mouse cursor over a plot, various tools become visible that allow, for example, zooming and panning.
![](pictures/Tools%20(Zoom).png)
In the _Overview_ tab on the _Alignment_ page, there is also a selection tool that allows you to select specific sequences. For the chosen range, a table is generated that shows, among other information, the contained sequences. You can click on a sequence to automatically copy it to the search line of the _Search_ tab.
![](pictures/Select.png)
In the _Search_ tab you can search for further occurrences of selected sequences by specifying the maximum Levenshtein distance. The reads containing these sequences can be selected to export them. 
![](pictures/Search.png)
To export selected reads as new BAM or FAST5 files, the output path and file names must be specified on the _Import | Export_ page.
![](pictures/Export.png)

