#
#  app.py
#
#
#  Created by Jacob Pollack.
#

import os
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import h5py
import pysam
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import plots
import tables

# descriptions of bitwise flags
DESCRIPTIONS = ["paired",
                "proper pair",
                "unmapped",
                "mate unmapped",
                "reverse",
                "mate reverse",
                "read1",
                "read2",
                "secondary",
                "qcfail",
                "duplicate",
                "supplementary"]

# CIGAR operations with description
OPERATIONS = ["M (alignment match)",
              "I (insertion)",
              "D (deletion)",
              "N (skipped region)",
              "S (soft clipping)",
              "H (hard clipping)",
              "P (padding)",
              "= (sequence match)",
              "X (sequence mismatch)"]

# ----------------------------------------------------------------------------------------------------------------------
#                                       S I D E B A R   L A Y O U T
# ----------------------------------------------------------------------------------------------------------------------

app = dash.Dash(external_stylesheets=["https://codepen.io/chriddyp/pen/bWLwgP.css", dbc.themes.BOOTSTRAP])
app.config.suppress_callback_exceptions = True

SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.H2("BlaPPy", className="display-4"),
        html.Hr(),
        html.P(
            "Visual quality control for mapped sequencing data", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Import | Export", href="/", active="exact"),
                dbc.NavLink("Statistics", href="/statistics", active="exact"),
                dbc.NavLink("Alignments", href="/alignments", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div(id="page_content", style=CONTENT_STYLE)

app.layout = html.Div([dcc.Store(id="bam_path", storage_type="local"),
                       dcc.Store(id="fast5_path", storage_type="local"),
                       dcc.Store(id="selected_reads", storage_type="local"),
                       dcc.Store(id="overview_data", storage_type="memory"),
                       dcc.Location(id="url"), sidebar, content])

# ----------------------------------------------------------------------------------------------------------------------
#                                       I M P O R T   |   E X P O R T   L A Y O U T
# ----------------------------------------------------------------------------------------------------------------------

path_input = dbc.FormGroup(
    [dbc.Label("Input path (BAM)"),
     dbc.Input(id="path_input", persistence=True),
     ]
)
fast5_path_input = dbc.FormGroup(
    [dbc.Label("Input path (FAST5)"),
     dbc.Input(id="fast5_path_input", persistence=True),
     ]
)
output_path_input = dbc.FormGroup(
    [dbc.Label("Output path"),
     dbc.Input(id="output_path_input", persistence=True),
     ]
)
output_filename_input = dbc.FormGroup(
    [dbc.Label("Filename (BAM)"),
     dbc.Input(id="output_filename_input", persistence=True),
     ]
)
fast5_output_filename_input = dbc.FormGroup(
    [dbc.Label("Filename (FAST5)"),
     dbc.Input(id="fast5_output_filename_input", persistence=True),
     ]
)
selected_reads_table = dash_table.DataTable(id="selected_reads_table",
                                            style_cell={"font-family": "Courier New, monospace"})
selected_reads_table_container = html.Div(id="selected_reads_table_container",
                                          children=[selected_reads_table]
                                          )
export_button = dbc.Button("Export", id="export_button")
# clear_button = dbc.Button("Clear", id="clear_button")
export_button_container = html.Div(id="export_button_container",
                                   children=[export_button]
                                   )
export_alert = html.Div(id="export_alert")

import_export_page = html.Div([html.Br(),
                               path_input,
                               fast5_path_input,
                               html.Hr(),
                               html.Br(),
                               dbc.Row([dbc.Col(output_path_input),
                                        dbc.Col(output_filename_input, width=3)
                                        ]),
                               dbc.Row([dbc.Col(),
                                        dbc.Col(fast5_output_filename_input, width=3)
                                        ]),
                               selected_reads_table_container,
                               export_button_container,
                               export_alert
                               ])

# ----------------------------------------------------------------------------------------------------------------------
#                                       S T A T I S T I C S   L A Y O U T
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# R E F E R E N C E S   T A B
# --------------------------------------
references_plot = dcc.Graph(id="references_plot")

references_tab = dbc.Tab(children=[html.Br(),
                                   references_plot
                                   ],
                         label="References"
                         )

# --------------------------------------
# C O V E R A G E   T A B
# --------------------------------------
coverage_reference_dropdown = dbc.FormGroup([dbc.Label("Reference"),
                                             dbc.Select(id="coverage_reference_dropdown", persistence=True)
                                             ])
coverage_number_of_bars_input = dbc.FormGroup(
    [dbc.Label("Number of bars"),
     dbc.Input(id="coverage_number_of_bars_input", type="number", value=30, min=1, persistence=True)
     ]
)
coverage_plot = dcc.Graph(id="coverage_plot")

coverage_tab = dbc.Tab(children=[html.Br(),
                                 dbc.Row([dbc.Col(coverage_reference_dropdown),
                                          dbc.Col(coverage_number_of_bars_input, width=3)
                                          ]),
                                 coverage_plot
                                 ],
                       label="Coverage"
                       )

# --------------------------------------
# B I T W I S E   F L A G S   T A B
# --------------------------------------
bitwise_flags_switch = dbc.Checklist(
    options=[
        {"label": "distinguish references", "value": "distinguish references"},
    ],
    value=[],
    id="bitwise_flags_switch",
    switch=True,
    persistence=True
)
bitwise_flags_multiple_references_dropdown = dcc.Dropdown(id="bitwise_flags_multiple_references_dropdown",
                                                          multi=True,
                                                          value=[],
                                                          persistence=True
                                                          )
bitwise_flags_multiple_references_plot = dcc.Graph(id="bitwise_flags_multiple_references_plot")
bitwise_flags_reference_dropdown = dbc.FormGroup([dbc.Label("Reference"),
                                                  dbc.Select(id="bitwise_flags_reference_dropdown", persistence=True)
                                                  ])
bit_dropdown = dbc.FormGroup([dbc.Label("Bit"), dbc.Select(
    id="bit_dropdown",
    options=[{"label": description, "value": description} for description in DESCRIPTIONS],
    value=DESCRIPTIONS[0],
    persistence=True
)
                              ])
bitwise_flags_number_of_bars_input = dbc.FormGroup(
    [dbc.Label("Number of bars"),
     dbc.Input(id="bitwise_flags_number_of_bars_input", type="number", value=30, min=1, persistence=True)
     ]
)
bitwise_flags_single_reference_plot = dcc.Graph(id="bitwise_flags_single_reference_plot")

bitewise_flags_tab = dbc.Tab(children=[html.Br(),
                                       bitwise_flags_multiple_references_dropdown,
                                       bitwise_flags_switch,
                                       bitwise_flags_multiple_references_plot,
                                       html.Hr(),
                                       html.Br(),
                                       dbc.Row([
                                           dbc.Col(bitwise_flags_reference_dropdown),
                                           dbc.Col(bit_dropdown),
                                           dbc.Col(bitwise_flags_number_of_bars_input, width=3)
                                       ]),
                                       bitwise_flags_single_reference_plot
                                       ],
                             label="Bitwise flags"
                             )

# --------------------------------------
# C I G A R   S T R I N G S   T A B
# --------------------------------------
cigar_strings_switch = dbc.Checklist(
    options=[
        {"label": "distinguish references", "value": "distinguish references"},
    ],
    value=[],
    id="cigar_strings_switch",
    switch=True,
    persistence=True
)
cigar_strings_multiple_references_dropdown = dcc.Dropdown(id="cigar_strings_multiple_references_dropdown",
                                                          multi=True,
                                                          value=[],
                                                          persistence=True
                                                          )
cigar_strings_multiple_references_plot = dcc.Graph(id="cigar_strings_multiple_references_plot")
cigar_strings_reference_dropdown = dbc.FormGroup([dbc.Label("Reference"),
                                                  dbc.Select(id="cigar_strings_reference_dropdown", persistence=True)
                                                  ])
cigar_operation_dropdown = dbc.FormGroup(
    [dbc.Label("CIGAR operation"),
     dbc.Select(id="cigar_operation_dropdown",
                options=[{"label": operation, "value": operation[0]} for operation in OPERATIONS],
                value=OPERATIONS[0][0],
                persistence=True
                )
     ]
)
cigar_strings_number_of_bars_input = dbc.FormGroup(
    [dbc.Label("Number of bars"),
     dbc.Input(id="cigar_strings_number_of_bars_input", type="number", value=30, min=1, persistence=True)
     ]
)
cigar_strings_single_reference_plot = dcc.Graph(id="cigar_strings_single_reference_plot")

cigar_strings_tab = dbc.Tab(children=[html.Br(),
                                      cigar_strings_multiple_references_dropdown,
                                      cigar_strings_switch,
                                      cigar_strings_multiple_references_plot,
                                      html.Hr(),
                                      html.Br(),
                                      dbc.Row([
                                          dbc.Col(cigar_strings_reference_dropdown),
                                          dbc.Col(cigar_operation_dropdown),
                                          dbc.Col(cigar_strings_number_of_bars_input, width=3)
                                      ]),
                                      cigar_strings_single_reference_plot
                                      ],
                            label="CIGAR strings"
                            )

# --------------------------------------
# S T A T I S T I C S   P A G E
# --------------------------------------
statistics_page = html.Div([dbc.Tabs([references_tab, coverage_tab, bitewise_flags_tab, cigar_strings_tab])])

# ----------------------------------------------------------------------------------------------------------------------
#                                       A L I G N M E N T S   L A Y O U T
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# O V E R V I E W   T A B
# --------------------------------------
overview_reference_dropdown = dbc.FormGroup([dbc.Label("Reference"),
                                             dbc.Select(id="overview_reference_dropdown", persistence=True)
                                             ])
borders_switch = dbc.Checklist(
    options=[
        {"label": "show borders", "value": "show borders"},
    ],
    value=[],
    id="borders_switch",
    switch=True,
    persistence=True
)
generate_button = dbc.Button("Generate", id="generate_button")
overview_plot = dcc.Graph(id="overview_plot")
overview_plot_container = html.Div(id="overview_plot_container", children=[overview_plot])
selected_patterns_table = dash_table.DataTable(id="selected_patterns_table",
                                               columns=[{"name": column, "id": column} for column in
                                                        ["read", "selected sequence", "CIGAR pattern"]],
                                               style_cell={"font-family": "Courier New, monospace"}
                                               )
selected_patterns_table_container = html.Div(id="selected_patterns_table_container", children=[selected_patterns_table])
pattern_alert = html.Div(id="pattern_alert")

overview_tab = dbc.Tab(id="overview",
                       children=[html.Br(),
                                 overview_reference_dropdown,
                                 borders_switch,
                                 generate_button,
                                 overview_plot_container,
                                 html.Br(),
                                 selected_patterns_table_container,
                                 pattern_alert
                                 ],
                       label="Overview"
                       )

# --------------------------------------
# S E A R C H   T A B
# --------------------------------------
pattern_input = dbc.FormGroup([dbc.Label("Sequence"),
                               dbc.Input(id="pattern_input", type="text", persistence=True)
                               ])
distance_input = dbc.FormGroup([dbc.Label("Levenshtein distance"),
                                dbc.Input(
                                    id="distance_input",
                                    type="number",
                                    min=0, persistence=True
                                )])

search_result_table = dash_table.DataTable(id="search_result_table",
                                           style_cell={"font-family": "Courier New, monospace"}
                                           )
select_button = dbc.Button("Select", id="select_button")
search_result_table_container = html.Div(id="search_result_table_container",
                                         children=[search_result_table, select_button]
                                         )
select_alert = html.Div(id="select_alert")

select_tab = dbc.Tab(id="search",
                     children=[html.Br(),
                               dbc.Row([dbc.Col(pattern_input), dbc.Col(distance_input, width=3)]),
                               search_result_table_container,
                               select_alert
                               ],
                     label="Search"
                     )

# --------------------------------------
# D E T A I L   T A B
# --------------------------------------
multiple_reads_dropdown = dcc.Dropdown(id="multiple_reads_dropdown",
                                       multi=True,
                                       value=[],
                                       persistence=True
                                       )
parallel_coordinates_plot = dcc.Graph(id="parallel_coordinates_plot")
detail_tab = dbc.Tab(label="Detail",
                     children=[html.Br(),
                               multiple_reads_dropdown,
                               parallel_coordinates_plot
                               ]
                     )

# --------------------------------------
# A L I G N M E N T S   P A G E
# --------------------------------------
alignments_page = html.Div([dbc.Tabs(id="tabs", children=[overview_tab, select_tab, detail_tab])])


# ----------------------------------------------------------------------------------------------------------------------
#                                       S I D E B A R   C A L L B A C K S
# ----------------------------------------------------------------------------------------------------------------------

# Change page, if link in sidebar is clicked
@app.callback(Output("page_content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname == "/":
        return import_export_page
    elif pathname == "/statistics":
        return statistics_page
    elif pathname == "/alignments":
        return alignments_page
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


# ----------------------------------------------------------------------------------------------------------------------
#                                       I M P O R T   |   E X P O R T   C A L L B A C K S
# ----------------------------------------------------------------------------------------------------------------------

# check if path is invalid
@app.callback(Output("path_input", "invalid"), [Input("path_input", "value")])
def check_path(value):
    try:
        pysam.AlignmentFile(value, "r")
    except:
        return True
    return False


# store bam_path
@app.callback(Output("bam_path", "data"), [Input("path_input", "value")])
def store_bam_path(value):
    try:
        pysam.AlignmentFile(value, "r")
    except:
        return None
    return value


# check if fast5 path is invalid
@app.callback(Output("fast5_path_input", "invalid"), [Input("fast5_path_input", "value")])
def check_path(value):
    try:
        h5py.File(value, "r")
    except:
        return True
    return False


# store fast5_path
@app.callback(Output("fast5_path", "data"), [Input("fast5_path_input", "value")])
def store_fast5_path(value):
    try:
        h5py.File(value, "r")
    except:
        return None
    return value


# clear selected_reads
# @app.callback(Output("selected_reads", "clear_data"), [Input("path_input", "value")])
# def clear_selected_reads(path):
# return True


# clear overview_data
# @app.callback(Output("overview_data", "clear_data"), [Input("path_input", "value")])
# def clear_overview_data(path):
# return True


# check if output path is invalid
@app.callback(Output("output_path_input", "invalid"),
              [Input("output_path_input", "value")]
              )
def check_path(path):
    if path is None:
        return True
    return not os.path.isdir(path)


# check if output filename is invalid
@app.callback(Output("output_filename_input", "invalid"),
              [Input("output_filename_input", "value"), Input("output_path_input", "value")]
              )
def check_filname(filename, path):
    if path is None or filename is None:
        return True

    # check if file already exists
    if os.path.exists(os.path.join(path, filename)):
        return True

    # check if suffix is correct
    return not filename.endswith(".bam")


# check if fast5 output filename is invalid
@app.callback(Output("fast5_output_filename_input", "invalid"),
              [Input("fast5_output_filename_input", "value"), Input("output_path_input", "value")]
              )
def check_filname(filename, path):
    if path is None or filename is None:
        return True

    # check if file already exists
    if os.path.exists(os.path.join(path, filename)):
        return True

    # check if suffix is correct
    return not filename.endswith(".fast5")


# update table, if stored selected reads changed or page reloaded
@app.callback(
    Output("selected_reads_table_container", "children"),
    [Input("selected_reads", "data"), Input("url", "pathname")],
)
def update_selected_reads_table(selected_reads, page):
    if selected_reads is not None:
        selected_reads_table = tables.selected_reads_table(selected_reads)
        return [selected_reads_table]
    else:
        raise PreventUpdate


# handle export
@app.callback(
    Output("export_alert", "children"),
    [Input("export_button", "n_clicks")],
    [State("output_path_input", "invalid"),
     State("output_filename_input", "invalid"),
     State("fast5_output_filename_input", "invalid"),
     State("output_path_input", "value"),
     State("output_filename_input", "value"),
     State("fast5_output_filename_input", "value"),
     State("selected_reads_table", "data"),
     State("bam_path", "data"),
     State("fast5_path", "data"),
     ])
def export(n_clicks,
           path_invalid, filename_invalid, fast5_filename_invalid,
           path, filename, fast5_filename,
           data, bam_path, fast5_path
           ):
    if n_clicks is not None:

        success = False

        if not path_invalid and not filename_invalid:
            # create file
            bam_file = pysam.AlignmentFile(bam_path, "rb")

            # create index to search reads
            index = pysam.IndexedReads(bam_file)
            index.build()

            # create new file
            output_path = os.path.join(path, filename)
            new_bam_file = pysam.AlignmentFile(output_path, "wb", template=bam_file)
            for row in data:
                for read in index.find(row["selected reads"]):
                    new_bam_file.write(read)
            new_bam_file.close()

            # sort and index the new file
            sorted_output_path = output_path.replace(".bam", "_sorted.bam")
            pysam.sort("-o", sorted_output_path, output_path)
            pysam.index(sorted_output_path)
            success = True

        if not path_invalid and not fast5_filename_invalid:
            # create new fast5 file
            fast5_output_path = os.path.join(path, fast5_filename)
            fast5_file = h5py.File(fast5_path, "r")
            new_file = h5py.File(fast5_output_path, "w")
            for row in data:
                fast5_file.copy("read_" + row["selected reads"], new_file)

        if success:
            return dbc.Alert("Export successful", color="success", duration=4000)


# only show buttons, if there are results and output path is valid
@app.callback(
    Output("export_button_container", "style"),
    [Input("selected_reads_table", "data"),
     Input("output_path_input", "invalid"),
     Input("output_filename_input", "invalid")]
)
def toggle_container(data, path_invalid, filename_invalid):
    if data is None or path_invalid or filename_invalid:
        return {"display": "none"}
    else:
        return {"display": "block"}


# ----------------------------------------------------------------------------------------------------------------------
#                                       S T A T I S T I C S   C A L L B A C K S
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# R E F E R E N C E S   C A L L B A C K S
# --------------------------------------

# update references_plot, if path changed
@app.callback(Output("references_plot", "figure"), [Input("bam_path", "data")])
def update_references_plot(bam_path):
    return plots.references_plot(bam_path)


# --------------------------------------
# C O V E R A G E   C A L L B A C K S
# --------------------------------------

# update reference dropdown options, if path changed
@app.callback(Output("coverage_reference_dropdown", "options"),
              [Input("bam_path", "data")])
def update_reference_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]
    return [{"label": reference, "value": reference} for reference in references]


# update reference dropdown value, if path changed
# @app.callback(Output("coverage_reference_dropdown", "value"),
# [Input("coverage_reference_dropdown", "options")],
# [State("coverage_reference_dropdown", "value")])
# def update_reference_dropdown(options, value):
# print(value)
# if value is None:
# return [dict["value"] for dict in options][0]
# else:
# raise PreventUpdate


# update coverage_plot, if path, reference or number of bars changed
@app.callback(Output("coverage_plot", "figure"),
              [Input("bam_path", "data"),
               Input("coverage_reference_dropdown", "value"),
               Input("coverage_number_of_bars_input", "value")
               ]
              )
def update_coverage_plot(bam_path, reference, number_of_bars):
    if reference is None:
        raise PreventUpdate
    else:
        return plots.coverage_plot(bam_path, reference, number_of_bars)


# --------------------------------------
# B I T W I S E   F L A G S   C A L L B A C K S
# --------------------------------------

# update multiple references dropdown options, if path changed
@app.callback(Output("bitwise_flags_multiple_references_dropdown", "options"), [Input("bam_path", "data")])
def update_multiple_references_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]

    return [{"label": reference, "value": reference} for reference in references]


# update multiple references dropdown value, if path changed
# @app.callback(Output("bitwise_flags_multiple_references_dropdown", "value"),
# [Input("bitwise_flags_multiple_references_dropdown", "options")]
# )
# def update_multiple_references_dropdown(options):
# only use the first 8 references by default, otherwise the plot is too confusing
# return [dict["value"] for dict in options[:8]]


# disable multiple references dropdown value, if switch changed
@app.callback(Output("bitwise_flags_multiple_references_dropdown", "disabled"),
              [Input("bitwise_flags_switch", "value")]
              )
def update_multiple_references_dropdown(value):
    if "distinguish references" in value:
        disable = False
    else:
        disable = True
    return disable


# update multiple references plot, if multiple references dropdown value or switch changed
@app.callback(
    Output("bitwise_flags_multiple_references_plot", "figure"),
    [
        Input("bitwise_flags_multiple_references_dropdown", "value"),
        Input("bam_path", "data"),
        Input("bitwise_flags_switch", "value")
    ]
)
def update_multiple_references_plot(references, bam_path, value):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")
    if "distinguish references" in value:
        distinguish = True
    else:
        distinguish = False
    return plots.bitwise_flags_multiple_references_plot(bam_file, references, distinguish)


# update reference dropdown options, if path changed
@app.callback(Output("bitwise_flags_reference_dropdown", "options"), [Input("bam_path", "data")])
def update_reference_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]

    return [{"label": reference, "value": reference} for reference in references]


# update reference dropdown value, if path changed
# @app.callback(Output("bitwise_flags_reference_dropdown", "value"),
# [Input("bitwise_flags_reference_dropdown", "options")])
# def update_reference_dropdown(options):
# return [dict["value"] for dict in options][0]


# update single reference plot, if path, reference, number of bars or bit changed
@app.callback(
    Output("bitwise_flags_single_reference_plot", "figure"),
    [
        Input("bam_path", "data"),
        Input("bitwise_flags_reference_dropdown", "value"),
        Input("bit_dropdown", "value"),
        Input("bitwise_flags_number_of_bars_input", "value")
    ]
)
def update_single_references_plot(bam_path, reference, bit, number_of_bars):
    if reference is None:
        raise PreventUpdate
    else:
        # create file
        bam_file = pysam.AlignmentFile(bam_path, "r")

        return plots.bitwise_flags_single_reference_plot(bam_file, reference, number_of_bars, bit)


# --------------------------------------
# C I G A R   S T R I N G S   C A L L B A C K S
# --------------------------------------

# update multiple references dropdown options, if path changed
@app.callback(Output("cigar_strings_multiple_references_dropdown", "options"), [Input("bam_path", "data")])
def update_multiple_references_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]

    return [{"label": reference, "value": reference} for reference in references]


# update multiple references dropdown value, if path changed
# @app.callback(Output("cigar_strings_multiple_references_dropdown", "value"),
# [Input("cigar_strings_multiple_references_dropdown", "options")]
# )
# def update_multiple_references_dropdown(options):
# only use the first 8 references by default, otherwise the plot is too confusing
# return [dict["value"] for dict in options[:8]]


# disable multiple references dropdown value, if switch changed
@app.callback(Output("cigar_strings_multiple_references_dropdown", "disabled"),
              [Input("cigar_strings_switch", "value")]
              )
def update_multiple_references_dropdown(value):
    if "distinguish references" in value:
        disable = False
    else:
        disable = True
    return disable


# update multiple references plot, if multiple references dropdown value or switch changed
@app.callback(
    Output("cigar_strings_multiple_references_plot", "figure"),
    [
        Input("cigar_strings_multiple_references_dropdown", "value"),
        Input("bam_path", "data"),
        Input("cigar_strings_switch", "value")
    ]
)
def update_multiple_references_plot(references, bam_path, value):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")
    if "distinguish references" in value:
        distinguish = True
    else:
        distinguish = False
    return plots.cigar_strings_multiple_references_plot(bam_file, references, distinguish)


# update reference dropdown options, if path changed
@app.callback(Output("cigar_strings_reference_dropdown", "options"), [Input("bam_path", "data")])
def update_reference_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]

    return [{"label": reference, "value": reference} for reference in references]


# update reference dropdown value, if path changed
# @app.callback(Output("cigar_strings_reference_dropdown", "value"),
# [Input("cigar_strings_reference_dropdown", "options")])
# def update_reference_dropdown(options):
# return [dict["value"] for dict in options][0]


# update single reference plot, if path, reference, number of bars or operation changed
@app.callback(
    Output("cigar_strings_single_reference_plot", "figure"),
    [
        Input("bam_path", "data"),
        Input("cigar_strings_reference_dropdown", "value"),
        Input("cigar_operation_dropdown", "value"),
        Input("cigar_strings_number_of_bars_input", "value")
    ]
)
def update_single_references_plot(bam_path, reference, operation, number_of_bars):
    if reference is None:
        raise PreventUpdate
    else:
        # create file
        bam_file = pysam.AlignmentFile(bam_path, "r")

        return plots.cigar_strings_single_reference_plot(bam_file, reference, operation, number_of_bars)


# ----------------------------------------------------------------------------------------------------------------------
#                                       A L I G N M E N T S   C A L L B A C K S
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# O V E R V I E W   C A L L B A C K S
# --------------------------------------

# update reference dropdown options, if path changed
@app.callback(Output("overview_reference_dropdown", "options"), [Input("bam_path", "data")])
def update_reference_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]

    return [{"label": reference, "value": reference} for reference in references]


# update reference dropdown value, if path changed
# @app.callback(Output("overview_reference_dropdown", "value"),
# [Input("overview_reference_dropdown", "options")])
# def update_reference_dropdown(options):
# return [dict["value"] for dict in options][0]


# store figure and maps, if button clicked
@app.callback(Output("overview_data", "data"),
              [Input("generate_button", "n_clicks")],
              [
                  State("bam_path", "data"),
                  State("overview_reference_dropdown", "value"),
                  State("borders_switch", "value")
              ])
def store_overview_plot(n_clicks, bam_path, reference, value):
    if n_clicks is not None:
        # create file
        bam_file = pysam.AlignmentFile(bam_path, "r")

        if "show borders" in value:
            borders = True
        else:
            borders = False

        return plots.generate_overview(bam_file, reference, borders)
    else:
        raise PreventUpdate


# update plot with stored figure, if stored figure changed or page reloaded
@app.callback(
    Output("overview_plot_container", "children"),
    [Input("overview_data", "data"), Input("url", "pathname")],
)
def update_overview_plot(overview_data, page):
    if overview_data is not None:
        # load stored figure
        overview_plot = overview_data["figure"]
        return [dcc.Graph(id="overview_plot", figure=overview_plot)]
    else:
        raise PreventUpdate


# update table, if patterns selected
@app.callback(
    Output("selected_patterns_table", "data"),
    [Input("overview_plot", "selectedData"), Input("overview_data", "data")]
)
def update_selected_patterns_table(selected_data, overview_data):
    if selected_data is not None and overview_data is not None:
        # keys are changed from int to string, when stored
        heat_map = [{int(key): value for key, value in row.items()} for row in overview_data["heat_map"]]
        read_map = [{int(key): value for key, value in row.items()} for row in overview_data["read_map"]]
        insertion_map = [{int(key): value for key, value in row.items()} for row in overview_data["insertion_map"]]

        # load stored maps
        return tables.selected_patterns_table(selected_data, heat_map, read_map, insertion_map)


# only show data table, if there are results
@app.callback(
    Output("selected_patterns_table_container", "style"),
    [Input("overview_plot", "selectedData")]
)
def toggle_table(selected_data):
    if selected_data is None:
        return {"display": "none"}
    else:
        return {"display": "block"}


# copy selected patterns to search line
@app.callback(Output("pattern_input", "value"),
              [Input("selected_patterns_table", "active_cell")],
              [State("selected_patterns_table", "data")]
              )
def update_pattern_input(active_cell, data):
    if active_cell is not None:
        row = active_cell["row"]
        column = active_cell["column_id"]
        return data[row][column].replace("_", "")
    else:
        raise PreventUpdate


# alert, if pattern copyed
@app.callback(Output("pattern_alert", "children"),
              [Input("selected_patterns_table", "active_cell")],
              )
def alert(active_cell):
    if active_cell is not None:
        return dbc.Alert("Pattern added to search line", color="success", duration=4000)


# @app.callback(Output("tabs", "active_tab"), [Input("selected_patterns_table", "active_cell")])
# def change_tab(active_cell):
# if active_cell:
# return "search"
# else:
# raise PreventUpdate


# update multiple reads dropdown options, if path changed
@app.callback(Output("multiple_reads_dropdown", "options"),
              [Input("bam_path", "data")]
              )
def update_multiple_references_dropdown(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    return [{"label": read, "value": read} for read in [read.query_name for read in bam_file.fetch()]]


# update reads dropdown, if alignment clicked
@app.callback(Output("multiple_reads_dropdown", "value"),
              [Input("overview_plot", "clickData")],
              [State("multiple_reads_dropdown", "value"), State("overview_data", "data")]
              )
def update_multiple_reads_dropdown(click_data, reads, overview_data):
    value = reads.copy()
    if click_data is not None:
        i = int(click_data["points"][0]["curveNumber"]) - 1

        # check if clicked curve is a read and not an insertion
        if i < len(overview_data["read_traces_query_names"]):
            value.append(overview_data["read_traces_query_names"][i])
        return value
    else:
        raise PreventUpdate


# --------------------------------------
# S E A R C H   C A L L B A C K S
# --------------------------------------

# search and update table, if pattern or input changed
@app.callback(
    Output("search_result_table_container", "children"),
    [Input("bam_path", "data"), Input("pattern_input", "value"), Input("distance_input", "value")]
)
def search(bam_path, pattern, distance):
    search_result_table = tables.search_result_table(bam_path, pattern, distance)
    return [search_result_table, select_button]


# update stored selected reads, if table changed
# @app.callback(Output("selected_reads", "data"),
# [Input("selected_reads_table", "data")]
# )
# def store_selected_reads(data):
# return [row["selected reads"] for row in data]

# handle clear
# @app.callback(
# Output("selected_reads", "data"),
# [Input("clear_button", "n_clicks")]
# )
# def clear(n_clicks):
# if n_clicks is not None:
# return None
# else:
# raise PreventUpdate

# store selected reads, if button clicked
@app.callback(Output("selected_reads", "data"),
              [Input("select_button", "n_clicks")],
              [State("selected_reads", "data"), State("search_result_table", "data")]
              )
def store_selected_reads(select_clicks, selected_reads, data):
    if dash.callback_context.triggered[0]['prop_id'].split('.')[0] == "select_button":
        if select_clicks is not None:
            # append new reads and remove duplicates
            new_reads = [row["read"] for row in data]
            if selected_reads is None:
                updated_selected_reads = list(dict.fromkeys(new_reads))
            else:
                updated_selected_reads = list(dict.fromkeys(selected_reads + new_reads))
            return updated_selected_reads
        else:
            raise PreventUpdate


# alert, if selected
@app.callback(Output("select_alert", "children"),
              [Input("select_button", "n_clicks")],
              )
def alert(n_clicks):
    if n_clicks is not None:
        return dbc.Alert("Selected reads added to export page", color="success", duration=4000)


# only show data table and button, if there are results
@app.callback(
    Output("search_result_table_container", "style"),
    [Input("pattern_input", "value"), Input("distance_input", "value")]
)
def toggle_table(pattern, distance):
    if pattern is None or distance is None:
        return {"display": "none"}
    else:
        return {"display": "block"}


# --------------------------------------
# D E T A I L   C A L L B A C K S
# --------------------------------------

# update plot, if reads dropdown value changed
@app.callback(
    Output("parallel_coordinates_plot", "figure"),
    [
        Input("multiple_reads_dropdown", "value"),
        Input("bam_path", "data"),
    ]
)
def update_parallel_coordinates_plot(query_names, bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # create index
    index = pysam.IndexedReads(bam_file)
    index.build()

    return plots.parallel_coordinates_plot(index, query_names)


if __name__ == "__main__":
    app.run_server(debug=False, port=8888)
