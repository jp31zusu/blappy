#
#  plots.py
#
#
#  Created by Jacob Pollack.
#

import plotly.graph_objects as go
import pysam
from plotly.subplots import make_subplots

LEGEND_COLORS = {
    "A": "rgba(0,255,0,0.5)",
    "C": "rgba(0,0,255,0.5)",
    "G": "rgba(255,255,0,0.5)",
    "T": "rgba(0,255,255,0.5)",
    "I": "rgba(255,0,0,0.5)",
    "D": "rgba(0,0,0,0.5)",
    "N": "rgba(127,127,127,0.5)"
}

# descriptions of bitwise flags
DESCRIPTIONS = ["paired",
                "proper pair",
                "unmapped",
                "mate unmapped",
                "reverse",
                "mate reverse",
                "read1",
                "read2",
                "secondary",
                "qcfail",
                "duplicate",
                "supplementary"]

# CIGAR operations with description
CIGAR_DESCRIPTIONS = ["M (alignment match)",
                      "I (insertion)",
                      "D (deletion)",
                      "N (skipped region)",
                      "S (soft clipping)",
                      "H (hard clipping)",
                      "P (padding)",
                      "= (sequence match)",
                      "X (sequence mismatch)"]


# ----------------------------------------------------------------------------------------------------------------------
#                                       S T A T I S T I C S   P L O T S
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# R E F E R E N C E S   P L O T S
# --------------------------------------
def references_plot(bam_path):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # ignore references without alignments
    references = [reference for reference in bam_file.references if bam_file.count(reference) > 0]

    # count base positions and alignments per reference
    base_positions_counts = [bam_file.get_reference_length(reference) for reference in references]
    alignments_counts = [bam_file.count(reference) for reference in references]

    # print average and max reference length
    #sum = 0
    #max = 0
    #for count in base_positions_counts:
        #sum += count
        #if count > max: max = count
    #mean = sum / len(base_positions_counts)
    #print(max)
    #print(mean)

    # print average and max nubmer of alignments
    #sum = 0
    #max = 0
    #for count in alignments_counts:
        #sum += count
        #if count > max: max = count
    #mean = sum / len(alignments_counts)
    #print(max)
    #print(mean)

    # create and return figure
    fig = go.Figure(data=[
        go.Bar(name="bases", x=references, y=base_positions_counts),
        go.Bar(name="alignments", x=references, y=alignments_counts)
    ])
    fig.update_xaxes(title="reference")
    fig.update_yaxes(title="count", type="log")
    fig.update_layout(barmode='group')
    return fig


# --------------------------------------
# C O V E R A G E   P L O T S
# --------------------------------------
def coverage_plot(bam_path, reference, number_of_bars):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    bar_positions = []
    bar_numreads = []
    bar_percent_covered = []
    bar_width = int(bam_file.get_reference_length(reference) / number_of_bars)

    # calculate percent covered for every bar
    for i in range(1, bam_file.get_reference_length(reference), bar_width):
        stats = pysam.coverage(bam_path, "-r", reference + ":" + str(i) + "-" + str(i + bar_width))
        numreads = float(stats.split("\n")[1].split("\t")[3])
        percent_covered = float(stats.split("\n")[1].split("\t")[5])
        bar_positions.append(i + bar_width / 2)
        bar_numreads.append(numreads)
        bar_percent_covered.append(percent_covered)

    # create and return barplots
    fig = make_subplots(rows=1, cols=2)
    fig.add_trace(
        go.Bar(name="", x=bar_positions, y=bar_percent_covered),
        row=1, col=1
    )
    fig.update_xaxes(title_text="reference position (bp)", row=1, col=1)
    fig.update_yaxes(title_text="percentage covered", row=1, col=1)
    fig.add_trace(
        go.Bar(name="", x=bar_positions, y=bar_numreads),
        row=1, col=2
    )
    fig.update_xaxes(title_text="reference position (bp)", row=1, col=2)
    fig.update_yaxes(title_text="number of read alignments", row=1, col=2)
    fig.update_layout(showlegend=False)
    return fig


# --------------------------------------
# B I T W I S E   F L A G S   P L O T S
# --------------------------------------
def bitwise_flags_multiple_references_plot(bam_file, references, distinguish):
    traces = []
    if (distinguish):

        # count alignments with bit set for every reference and every bit
        for reference in references:
            X = DESCRIPTIONS.copy()
            Y = [0] * 12
            count = bam_file.count(reference)
            for read in bam_file.fetch(reference):
                if read.is_paired:
                    Y[0] += 1
                if read.is_proper_pair:
                    Y[1] += 1
                if read.is_reverse:
                    Y[4] += 1
                if read.mate_is_reverse:
                    Y[5] += 1
                if read.is_read1:
                    Y[6] += 1
                if read.is_read2:
                    Y[7] += 1
                if read.is_secondary:
                    Y[8] += 1
                if read.is_qcfail:
                    Y[9] += 1
                if read.is_duplicate:
                    Y[10] += 1
                if read.is_supplementary:
                    Y[11] += 1

            # ignore bits with zero percentage
            for i in reversed(range(len(Y))):
                if Y[i] == 0:
                    del Y[i]
                    del X[i]
                else:
                    # calculate percentages
                    Y[i] = Y[i] / count * 100

            traces.append(go.Bar(name=reference, x=X, y=Y, legendgroup="references"))

    else:
        X = DESCRIPTIONS.copy()
        Y = [0] * 12
        count = bam_file.count()
        for read in bam_file.fetch():
            if read.is_paired:
                Y[0] += 1
            if read.is_proper_pair:
                Y[1] += 1
            if read.is_unmapped:
                Y[2] += 1
            if read.mate_is_unmapped:
                Y[3] += 1
            if read.is_reverse:
                Y[4] += 1
            if read.mate_is_reverse:
                Y[5] += 1
            if read.is_read1:
                Y[6] += 1
            if read.is_read2:
                Y[7] += 1
            if read.is_secondary:
                Y[8] += 1
            if read.is_qcfail:
                Y[9] += 1
            if read.is_duplicate:
                Y[10] += 1
            if read.is_supplementary:
                Y[11] += 1

        # ignore bits with zero percentage
        for i in reversed(range(len(Y))):
            if Y[i] == 0:
                del Y[i]
                del X[i]
            else:
                # calculate percentages
                Y[i] = Y[i] / count * 100

        traces.append(go.Bar(name="all references", x=X, y=Y))

    # create and return plot
    fig = go.Figure()
    for barplot in traces:
        fig.add_trace(barplot)
    fig.update_xaxes(title_text="bit")
    fig.update_yaxes(title_text="percentage of alignments with bit set")
    return fig


def bitwise_flags_single_reference_plot(bam_file, reference, number_of_bars, bit):
    bar_width = int(bam_file.get_reference_length(reference) / number_of_bars)
    X = []
    Y = []

    # for every bar count overlapping alignments with bit set
    for i in range(0, bam_file.get_reference_length(reference), bar_width):
        x = i + 1
        y = 0
        number_of_reads = 0
        for pileupcolumn in bam_file.pileup(reference, i, i + bar_width, truncate=True, stepper="nofilter"):
            for pileupread in pileupcolumn.pileups:
                number_of_reads += 1
                if bit == DESCRIPTIONS[0] and pileupread.alignment.is_paired:
                    y += 1
                elif bit == DESCRIPTIONS[1] and pileupread.alignment.is_proper_pair:
                    y += 1
                elif bit == DESCRIPTIONS[2] and pileupread.alignment.is_unmapped:
                    y += 1
                elif bit == DESCRIPTIONS[3] and pileupread.alignment.mate_is_unmapped:
                    y += 1
                elif bit == DESCRIPTIONS[4] and pileupread.alignment.is_reverse:
                    y += 1
                elif bit == DESCRIPTIONS[5] and pileupread.alignment.mate_is_reverse:
                    y += 1
                elif bit == DESCRIPTIONS[6] and pileupread.alignment.is_read1:
                    y += 1
                elif bit == DESCRIPTIONS[7] and pileupread.alignment.is_read2:
                    y += 1
                elif bit == DESCRIPTIONS[8] and pileupread.alignment.is_secondary:
                    y += 1
                elif bit == DESCRIPTIONS[9] and pileupread.alignment.is_qcfail:
                    y += 1
                elif bit == DESCRIPTIONS[10] and pileupread.alignment.is_duplicate:
                    y += 1
                elif bit == DESCRIPTIONS[11] and pileupread.alignment.is_supplementary:
                    y += 1
        X.append(x)

        # calculate percentages
        if (number_of_reads == 0):
            Y.append(0)
        else:
            Y.append(y / number_of_reads * 100)

    # count alignments with bit set and alignments with bit unset
    set = 0
    unset = 0
    for read in bam_file.fetch(reference):
        if bit == DESCRIPTIONS[0] and read.is_paired:
            set += 1
        elif bit == DESCRIPTIONS[1] and read.is_proper_pair:
            set += 1
        elif bit == DESCRIPTIONS[2] and read.is_unmapped:
            set += 1
        elif bit == DESCRIPTIONS[3] and read.mate_is_unmapped:
            set += 1
        elif bit == DESCRIPTIONS[4] and read.is_reverse:
            set += 1
        elif bit == DESCRIPTIONS[5] and read.mate_is_reverse:
            set += 1
        elif bit == DESCRIPTIONS[6] and read.is_read1:
            set += 1
        elif bit == DESCRIPTIONS[7] and read.is_read2:
            set += 1
        elif bit == DESCRIPTIONS[8] and read.is_secondary:
            set += 1
        elif bit == DESCRIPTIONS[9] and read.is_qcfail:
            set += 1
        elif bit == DESCRIPTIONS[10] and read.is_duplicate:
            set += 1
        elif bit == DESCRIPTIONS[11] and read.is_supplementary:
            set += 1
        else:
            unset += 1

    # create and return figure
    fig = make_subplots(rows=1, cols=2, column_widths=[0.75, 0.25])
    fig.add_trace(go.Bar(x=X, y=Y, name=""), row=1, col=1)
    fig.add_trace(go.Bar(x=["unset", "set"], y=[unset, set], name=""),
                  row=1,
                  col=2)
    fig.update_xaxes(title_text="reference position (bp)", row=1, col=1)
    fig.update_xaxes(title_text=bit, row=1, col=2)
    fig.update_yaxes(title_text="percentage of overlapping alignments with bit set", row=1, col=1)
    fig.update_yaxes(title_text="absolute number of alignments", row=1, col=2)
    fig.update_layout(showlegend=False)
    return fig


# --------------------------------------
# C I G A R   S T R I N G S   P L O T S
# --------------------------------------

def cigar_strings_multiple_references_plot(bam_file, references, distinguish):
    traces = []
    all_stats = []

    if (distinguish):
        # for every reference sum up the numbers of every CIGAR operation
        for reference in references:
            stats = list([0] * 11)
            for read in bam_file.fetch(reference):
                stats = [a + b for a, b in zip(stats, list(read.get_cigar_stats()[0]))]

            # calculate percentages
            stats = [stat / sum(stats) * 100 for stat in stats]
            stats = stats[:9]
            all_stats.append(stats)

        # ignore operations with zero percentage
        nonzero_operation_indices = []
        for i in range(len(CIGAR_DESCRIPTIONS)):
            if sum([stat[i] for stat in all_stats]) > 0:
                nonzero_operation_indices.append(i)
        nonzero_operations = [CIGAR_DESCRIPTIONS[i] for i in nonzero_operation_indices]
        all_stats = [[stat[i] for i in nonzero_operation_indices] for stat in all_stats]

        for i in range(len(references)):
            traces.append(go.Bar(name=references[i], x=nonzero_operations, y=all_stats[i]))

    else:
        stats = list([0] * 11)
        for read in bam_file.fetch():
            stats = [a + b for a, b in zip(stats, list(read.get_cigar_stats()[0]))]

        # calculate percentages
        stats = [stat / sum(stats) * 100 for stat in stats]
        stats = stats[:9]

        # ignore operations with zero percentage
        nonzero_operation_indices = []
        for i in range(len(CIGAR_DESCRIPTIONS)):
            if stats[i] > 0:
                nonzero_operation_indices.append(i)
        nonzero_operations = [CIGAR_DESCRIPTIONS[i] for i in nonzero_operation_indices]
        stats = [stats[i] for i in nonzero_operation_indices]

        traces.append(go.Bar(name="all references", x=nonzero_operations, y=stats))

    # create and return figure
    fig = go.Figure(traces)
    fig.update_xaxes(title_text="CIGAR operation")
    fig.update_yaxes(title_text="percentage of CIGAR strings")
    return fig


def cigar_strings_single_reference_plot(bam_file, reference, operation, number_of_bars):
    # calculate length of the longest CIGAR string
    max_read_length = 0
    for read in bam_file.fetch(reference):
        if not read.is_unmapped:
            if len(read.cigarstring) > max_read_length:
                max_read_length = len(read.cigarstring)

    read_positions = list(range(max_read_length))
    operation_counts = [0] * len(read_positions)
    total_counts = [0] * len(read_positions)

    # count occurrence of operation for each position
    for read in bam_file.fetch(reference):
        if not read.is_unmapped:
            for i in range(len(read.cigarstring)):
                if read.cigarstring[i] == operation:
                    operation_counts[i] += 1
                total_counts[i] += 1

    # count occurrence per bar
    bar_width = int(max_read_length / number_of_bars)
    X = []
    Y = []
    for i in range(0, len(read_positions), bar_width):
        x = read_positions[i]
        operation_count = 0
        total_count = 0
        for j in range(i, i + bar_width):
            if j < len(read_positions):
                operation_count += operation_counts[j]
                total_count += total_counts[j]
        # calculate percentage
        y = operation_count / total_count * 100
        X.append(x)
        Y.append(y)

    # create and return figure
    fig = go.Figure(go.Bar(x=X, y=Y, name=""))
    fig.update_xaxes(title_text="position in CIGAR string")
    fig.update_yaxes(title_text="percentage of sequence bases with CIGAR operation " + operation)
    return fig


# ----------------------------------------------------------------------------------------------------------------------
#                                       A L I G N M E N T S   P L O T S
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# O V E R V I E W   P L O T S
# --------------------------------------
def overview_plot(bam_file, reference):
    # list of dicts, where each dict corresponds to a row and each key corresponds to a column
    # stores occupied positions
    map = [dict()]

    # lists to store the x, y and z parameters for the heatmap
    X = []
    Y = []
    Z = []

    # lists to store insertion traces and read traces (read traces are transparent, but needed as hoverinfo)
    insertion_traces = []
    read_traces = []

    for read in bam_file.fetch(reference):
        if read.query_sequence is not None:

            # search minimal row with enough space for the current alignment
            row_found = False
            row = 0
            while not row_found:
                row_found = True
                for column in range(read.reference_start, read.reference_end):
                    if column in map[row]:
                        row_found = False
                        break
                if not row_found:
                    row += 1
                    if row == len(map):
                        map.append(dict())
                        row_found = True

            # save read traces
            read_traces.append(
                go.Scatter(x=[read.reference_start - 0.5,
                              read.reference_start - 0.5,
                              read.reference_end - 0.5,
                              read.reference_end - 0.5],
                           y=[row - 0.5,
                              row + 1 - 0.5,
                              row + 1 - 0.5,
                              row - 0.5],
                           mode="none",
                           fill="toself",
                           fillcolor="rgba(0, 0, 0, 0)",
                           hoverinfo="text",
                           text=read.query_name,
                           showlegend=False))

            # assemble new sequence by inserting D and N while ignoring I and S
            seq = ""
            count = 0
            for tuple in read.cigartuples:
                if tuple[0] == 0:
                    for i in range(tuple[1]):
                        seq += read.query_sequence[count + i]
                elif tuple[0] == 2:
                    for i in range(tuple[1]):
                        seq += "D"
                elif tuple[0] == 3:
                    for i in range(tuple[1]):
                        seq += "N"
                if tuple[0] == 1:

                    # save insertion trace
                    insertion_seq = ""
                    for i in range(tuple[1]):
                        insertion_seq += read.query_sequence[count + i]
                    x = read.reference_start + len(seq)
                    insertion_traces.append(

                        # draw I shape
                        go.Scatter(x=[x - 0.75, x - 0.75,
                                      x - 0.6, x - 0.6,
                                      x - 0.75, x - 0.75,
                                      x - 0.25, x - 0.25,
                                      x - 0.4, x - 0.4,
                                      x - 0.25, x - 0.25
                                      ],
                                   y=[row - 0.5,
                                      row + 0.2 - 0.5, row + 0.2 - 0.5,
                                      row + 0.8 - 0.5, row + 0.8 - 0.5,
                                      row + 1 - 0.5, row + 1 - 0.5,
                                      row + 0.8 - 0.5, row + 0.8 - 0.5,
                                      row + 0.2 - 0.5, row + 0.2 - 0.5,
                                      row - 0.5],
                                   mode="none",
                                   fill="toself",
                                   fillcolor="red",
                                   hoverinfo="text",
                                   text=insertion_seq,
                                   showlegend=False))

                if tuple[0] == 0 or tuple[0] == 1 or tuple[0] == 4:
                    count += tuple[1]

            # save heat values and reads
            for i in range(read.reference_length):
                if seq[i] == "A":
                    heat = 0
                elif seq[i] == "C":
                    heat = 1
                elif seq[i] == "G":
                    heat = 2
                elif seq[i] == "T":
                    heat = 3
                elif seq[i] == "D":
                    heat = 4
                elif seq[i] == "N":
                    heat = 5
                column = read.reference_start + i
                X.append(column)
                Y.append(row)
                Z.append(heat)
                map[row][column] = True

            # padding (this is needed, because otherwise the heatmap would strech bases over non-occuring x coordinates)
            X.append(read.reference_start - 1)
            Y.append(None)
            Z.append(None)
            X.append(read.reference_end + 1)
            Y.append(None)
            Z.append(None)

    # draw heatmap
    fig = go.Figure(go.Heatmap(x=X, y=Y, z=Z,
                               colorscale=[(0.00, LEGEND_COLORS["A"]), (0.166, LEGEND_COLORS["A"]),
                                           (0.166, LEGEND_COLORS["C"]), (0.33, LEGEND_COLORS["C"]),
                                           (0.33, LEGEND_COLORS["G"]), (0.5, LEGEND_COLORS["G"]),
                                           (0.5, LEGEND_COLORS["T"]), (0.66, LEGEND_COLORS["T"]),
                                           (0.66, LEGEND_COLORS["D"]), (0.833, LEGEND_COLORS["D"]),
                                           (0.833, LEGEND_COLORS["N"]), (1.00, LEGEND_COLORS["N"])],
                               showscale=False,
                               hoverinfo="none",
                               zmin=0, zmax=5))

    # add reads, insertions and legend traces
    for trace in read_traces:
        fig.add_trace(trace)
    for trace in insertion_traces:
        fig.add_trace(trace)

    # draw legend
    for base in LEGEND_COLORS.keys():
        fig.add_trace(go.Scatter(x=[0, 0],
                                 y=[0, 0],
                                 mode="markers",
                                 line=dict(color=LEGEND_COLORS[base]),
                                 name=base,
                                 hoverinfo="none"))

    # update axes and return plot
    fig.update_xaxes(title_text="reference position",
                     range=[0, bam_file.get_reference_length(reference)])  # , rangeslider=dict(visible=True))
    fig.update_yaxes(visible=False)
    return fig


def generate_overview(bam_file, reference, borders):
    # x, y and z parameter used for the heatmap
    X = []
    Y = []
    Z = []

    # lists of dicts, where each dict corresponds to a row and each key corresponds to a column
    heat_map = [dict()]  # stores heats of occupied positions
    read_map = [dict()]  # stores query names of the corresponding reads
    insertion_map = [dict()]  # stores insertion sequences

    # traces to mark insertions
    insertion_traces = []

    # traces of reads (these or invisible, but needed for hover info)
    read_traces = []

    # this is needed to select clicked reads
    read_traces_query_names = []

    for read in bam_file.fetch(reference, 0, bam_file.get_reference_length(reference)):
        if read.query_sequence is not None:

            # search minimal row with enough space for the current alignment
            row_found = False
            row = 0
            while not row_found:
                row_found = True
                for column in range(read.reference_start, read.reference_end):
                    if column in heat_map[row]:
                        row_found = False
                        break
                if not row_found:
                    row += 1
                    if row == len(heat_map):
                        heat_map.append(dict())
                        read_map.append(dict())
                        insertion_map.append(dict())
                        row_found = True

            # save read traces
            if borders:
                mode = "lines"
            else:
                mode = "none"
            read_traces.append(go.Scatter(x=[read.reference_start - 0.5,
                                             read.reference_start - 0.5,
                                             read.reference_end - 0.5,
                                             read.reference_end - 0.5,
                                             read.reference_start - 0.5],
                                          y=[row - 0.5,
                                             row + 1 - 0.5,
                                             row + 1 - 0.5,
                                             row - 0.5,
                                             row - 0.5],
                                          mode=mode,
                                          line_color="rgba(0, 0, 0, 0.5)",
                                          line_width=0.5,
                                          fill="toself",
                                          fillcolor="rgba(0, 0, 0, 0)",
                                          hoverinfo="text",
                                          text=read.query_name,
                                          showlegend=False))

            # save query name
            read_traces_query_names.append(read.query_name)

            # assemble new sequence by inserting D and N while ignoring I and S
            seq = ""
            count = 0
            for tuple in read.cigartuples:
                if tuple[0] == 0:
                    for i in range(tuple[1]):
                        seq += read.query_sequence[count + i]
                elif tuple[0] == 2:
                    for i in range(tuple[1]):
                        seq += "D"
                elif tuple[0] == 3:
                    for i in range(tuple[1]):
                        seq += "N"

                # save insertion traces
                if tuple[0] == 1:
                    insertion_seq = ""
                    for i in range(tuple[1]):
                        insertion_seq += read.query_sequence[count + i]
                    x = read.reference_start + len(seq)

                    insertion_map[row][x] = insertion_seq
                    # I shape
                    insertion_traces.append(go.Scatter(x=[x - 0.75,
                                                          x - 0.75,
                                                          x - 0.6,
                                                          x - 0.6,
                                                          x - 0.75,
                                                          x - 0.75,
                                                          x - 0.25,
                                                          x - 0.25,
                                                          x - 0.4,
                                                          x - 0.4,
                                                          x - 0.25,
                                                          x - 0.25],
                                                       y=[row - 0.5,
                                                          row + 0.2 - 0.5,
                                                          row + 0.2 - 0.5,
                                                          row + 0.8 - 0.5,
                                                          row + 0.8 - 0.5,
                                                          row + 1 - 0.5,
                                                          row + 1 - 0.5,
                                                          row + 0.8 - 0.5,
                                                          row + 0.8 - 0.5,
                                                          row + 0.2 - 0.5,
                                                          row + 0.2 - 0.5,
                                                          row - 0.5],
                                                       mode="none",
                                                       fill="toself",
                                                       fillcolor="red",
                                                       hoverinfo="text",
                                                       text=insertion_seq,
                                                       showlegend=False))

                if tuple[0] == 0 or tuple[0] == 1 or tuple[0] == 4:
                    count += tuple[1]

            # save heat values and reads to maps
            for i in range(read.reference_length):
                if seq[i] == "A":
                    heat = 0
                elif seq[i] == "C":
                    heat = 1
                elif seq[i] == "G":
                    heat = 2
                elif seq[i] == "T":
                    heat = 3
                elif seq[i] == "D":
                    heat = 4
                elif seq[i] == "N":
                    heat = 5
                column = read.reference_start + i
                heat_map[row][column] = heat
                X.append(column)
                Y.append(row)
                Z.append(heat)
                read_map[row][column] = read.query_name

            # padding (this is needed, because otherwise the Heatmap would strech bases over unspecified coordinates)
            X.append(read.reference_start - 1)
            Y.append(None)
            Z.append(None)
            X.append(read.reference_end + 1)
            Y.append(None)
            Z.append(None)

    # draw heatmap
    fig = go.Figure(go.Heatmap(x=X, y=Y, z=Z,
                               colorscale=[(0.00, LEGEND_COLORS["A"]), (0.166, LEGEND_COLORS["A"]),
                                           (0.166, LEGEND_COLORS["C"]), (0.33, LEGEND_COLORS["C"]),
                                           (0.33, LEGEND_COLORS["G"]), (0.5, LEGEND_COLORS["G"]),
                                           (0.5, LEGEND_COLORS["T"]), (0.66, LEGEND_COLORS["T"]),
                                           (0.66, LEGEND_COLORS["D"]), (0.833, LEGEND_COLORS["D"]),
                                           (0.833, LEGEND_COLORS["N"]), (1.00, LEGEND_COLORS["N"])],
                               showscale=False,
                               hoverinfo="none",
                               zmin=0, zmax=5))

    # draw reads, insertions and legend
    for trace in read_traces:
        fig.add_trace(trace)

    for trace in insertion_traces:
        fig.add_trace(trace)

    for base in LEGEND_COLORS.keys():
        fig.add_trace(go.Scatter(x=[0, 0],
                                 y=[0, 0],
                                 mode="markers",
                                 line=dict(color=LEGEND_COLORS[base]),
                                 name=base,
                                 hoverinfo="none"))

    fig.update_xaxes(title_text="reference position",
                     range=[0, bam_file.get_reference_length(reference)])
    fig.update_yaxes(visible=False)

    # fig.write_html("/Users/jacobpollack/Desktop/plot.html")

    # return data that can be reused for selection and click events and the plot
    data = dict()
    data["figure"] = fig
    data["heat_map"] = heat_map
    data["read_map"] = read_map
    data["insertion_map"] = insertion_map
    data["read_traces_query_names"] = read_traces_query_names

    return data


# --------------------------------------
# D E T A I L   P L O T S
# --------------------------------------

def parallel_coordinates_plot(index, query_names):
    if query_names == []:
        return go.Figure()

    # find relevant references
    references = set()
    for query_name in query_names:
        for read in index.find(query_name):
            if read.query_sequence is not None:
                references.add(read.reference_name)

    traces_X = dict()
    traces_Y = dict()
    for reference in references:
        for query_name in query_names:
            X = []
            Y = []
            for read in index.find(query_name):
                if read.query_sequence is not None and read.reference_name == reference:
                    if not reference in traces_X.keys():
                        traces_X[reference] = dict()
                        traces_Y[reference] = dict()

                    # assemble new sequence by inserting D and N while ignoring I and S
                    seq = ""
                    count = 0
                    for tuple in read.cigartuples:
                        if tuple[0] == 0:
                            for i in range(tuple[1]):
                                seq += read.query_sequence[count + i]
                        elif tuple[0] == 2:
                            for i in range(tuple[1]):
                                seq += "D"
                        elif tuple[0] == 3:
                            for i in range(tuple[1]):
                                seq += "N"

                        if tuple[0] == 0 or tuple[0] == 1 or tuple[0] == 4:
                            count += tuple[1]

                    for x in range(read.reference_start, read.reference_start + len(seq)):
                        X.append(x)
                    X.append(None)
                    for y in seq:
                        Y.append(y)
                    Y.append(None)

            if X != [] and Y != []:
                if not read.query_name in traces_X[reference].keys():
                    traces_X[reference][read.query_name] = []
                    traces_Y[reference][read.query_name] = []
                traces_X[reference][read.query_name].append(X)
                traces_Y[reference][read.query_name].append(Y)

    fig = make_subplots(rows=len(references),
                        cols=1,
                        shared_yaxes=True,
                        subplot_titles=list(references))

    row = 0
    for reference in references:
        row += 1
        for read in list(traces_X[reference].keys()):
            for i in range(len(traces_X[reference][read])):
                fig.add_trace(go.Scatter(x=traces_X[reference][read][i], y=traces_Y[reference][read][i],
                                         mode="lines",
                                         opacity=0.5,
                                         name=read),
                              row=row,
                              col=1
                              )

    fig.update_xaxes(rangeslider=dict(visible=True, thickness=0.15/len(references)))
    fig.update_yaxes(categoryorder='array', categoryarray=["A", "C", "G", "T", "D", "N"])
    fig.update_layout(height=450 * len(references))

    return fig
