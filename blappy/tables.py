#
#  tables.py
#
#
#  Created by Jacob Pollack.
#

import math
import dash_table
import pysam
from fuzzysearch import find_near_matches


# ----------------------------------------------------------------------------------------------------------------------
#                                       I M P O R T   |   E X P O R T   T A B L E S
# ----------------------------------------------------------------------------------------------------------------------
def selected_reads_table(selected_reads):
    # create data
    data = []
    for row in range(len(selected_reads)):
        data.append({"selected reads": selected_reads[row]})

    # create and return table
    table = dash_table.DataTable(
        id="selected_reads_table",
        columns=[{"name": "selected reads", "id": "selected reads"}],
        data=data,
        # row_deletable=True
    )
    return table


# ----------------------------------------------------------------------------------------------------------------------
#                                       A L I G N M E N T S   T A B L E S
# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------
# O V E R V I E W   T A B L E S
# --------------------------------------
def new_row(read, seq):
    # assemble CIGAR pattern from selected sequence
    cigar_seq = seq.replace(" ", "")
    for base in ["A", "C", "G", "T"]:
        cigar_seq = cigar_seq.replace(base, "M")

    for base in ["a", "c", "g", "t"]:
        cigar_seq = cigar_seq.replace(base, "I")

    cigar_string = ""
    count = 0
    current_element = cigar_seq[0]
    for element in cigar_seq:
        if element == current_element:
            count += 1
        else:
            cigar_string += str(count) + current_element
            count = 1
            current_element = element
    cigar_string += str(count) + current_element

    seq = seq.replace("D", "-")
    seq = seq.replace("N", ".")

    return [read, seq, cigar_string]


def add_spaces_for_insertions(sequences):
    # get maximal length
    max = 0
    for sequence in sequences:
        if len(sequence) > max:
            max = len(sequence)
    max *= len(sequences)  # upper bound for padded length

    # find positions with an insertion in any row
    for i in range(max):
        insertion_found = False
        for sequence in sequences:
            if i < len(sequence):
                if sequence[i] in ["a", "c", "g", "t"]:
                    insertion_found = True
                    break

        # if insertion found
        if insertion_found:
            # ...and any row has no insertion at this position
            for j in range(len(sequences)):
                sequence = sequences[j]
                if i < len(sequence):
                    if sequence[i] in ["A", "C", "G", "T", "-", "."]:
                        # ...add a space at this position
                        sequences[j] = sequence[:i] + "_" + sequence[i:]

    # upper insertions
    for i in range(len(sequences)):
        sequences[i] = sequences[i].replace("a", "A").replace("c", "C").replace("g", "G").replace("t", "T")

    return sequences


def selected_patterns_table(selected_data, heat_map, read_map, insertion_map):
    reads = []
    sequences = []
    cigar_strings = []

    # Handle select
    if selected_data is not None:

        # Calculate selected range
        x_min = math.ceil(selected_data["range"]["x"][0] + 0.5)
        x_max = math.floor(selected_data["range"]["x"][1] - 0.5)
        y_min = math.ceil(selected_data["range"]["y"][0] + 0.5)
        y_max = math.floor(selected_data["range"]["y"][1] - 0.5)

        if x_min <= x_max and y_min <= y_max:

            for y in range(y_max, y_min - 1, -1):
                # Restore selected sequence
                seq = ""
                if x_min in read_map[y]:
                    current_read = read_map[y][x_min]
                else:
                    current_read = None
                for x in range(x_min, x_max + 1):
                    if x in insertion_map[y]:
                        insertion = insertion_map[y][x]
                        seq += insertion.lower()

                    if x in read_map[y]:
                        next_read = read_map[y][x]
                    else:
                        next_read = None
                    if next_read != current_read:
                        if seq.replace(" ", "") != "":
                            # Save reads, sequences and CIGAR patterns
                            reads.append(new_row(current_read, seq)[0])
                            sequences.append(new_row(current_read, seq)[1])
                            cigar_strings.append(new_row(current_read, seq)[2])
                            seq = ""
                        current_read = next_read

                    if x in heat_map[y]:
                        heat = heat_map[y][x]
                        if heat == 0:
                            seq += "A"
                        elif heat == 1:
                            seq += "C"
                        elif heat == 2:
                            seq += "G"
                        elif heat == 3:
                            seq += "T"
                        elif heat == 4:
                            seq += "D"
                        elif heat == 5:
                            seq += "N"
                    else:
                        seq = " " + seq

                if seq.replace(" ", "") != "":
                    # Save reads, sequences and CIGAR patterns
                    reads.append(new_row(current_read, seq)[0])
                    sequences.append(new_row(current_read, seq)[1])
                    cigar_strings.append(new_row(current_read, seq)[2])

    sequences = add_spaces_for_insertions(sequences)

    data = []
    for row in range(len(reads)):
        data.append({"read": reads[row], "selected sequence": sequences[row], "CIGAR pattern": cigar_strings[row]})
    return data


# --------------------------------------
# S E A R C H   T A B L E S
# --------------------------------------
def search_result_table(bam_path, pattern, distance):
    # create file
    bam_file = pysam.AlignmentFile(bam_path, "r")

    # lists to store the columns
    query_names = []
    reference_names = []
    matched_sequences = []
    start_positions = []
    stop_positions = []
    distances = []

    if pattern is not None and distance is not None:

        # search pattern
        for read in bam_file.fetch():
            if read.query_sequence is not None:
                matches = find_near_matches(pattern, read.query_sequence, max_l_dist=distance)
                if matches != []:
                    # store columns
                    for match in matches:
                        query_names.append(read.query_name)
                        reference_names.append(read.reference_name)
                        matched_sequences.append(match.matched)
                        start_positions.append(match.start)
                        stop_positions.append(match.end)
                        distances.append(match.dist)

    # create data
    data = []
    for row in range(len(query_names)):
        data.append({"read": query_names[row],
                     "reference": reference_names[row],
                     "matched sequence": matched_sequences[row],
                     "start position": start_positions[row],
                     "stop position": stop_positions[row],
                     "levenshtein distance": distances[row]})

    # create and return table
    table = dash_table.DataTable(
        id="search_result_table",
        columns=[
            {"name": column, "id": column} for column in ["read",
                                                          "reference",
                                                          "matched sequence",
                                                          "start position",
                                                          "stop position",
                                                          "levenshtein distance"
                                                          ]
        ],
        data=data,
        row_deletable=True
    )
    return table
